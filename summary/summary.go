package summary

import (
	"encoding/json"
        "os"
	"path"
)

func Load(summaryPath string) (s Summary, err error) {
	content, err := os.ReadFile(path.Join(summaryPath, "metadata.json"))
	if err != nil {
		return
	}
	err = json.Unmarshal(content, &s)
	return
}


// this is the match metadata, it is read from the metadata.json
// file and contains the information needed to generate the summary
type Summary struct {
	TimeElapsed int         `json:"timeElapsed"`
	Players     []Player    `json:"playerStates"`
	MapSettings MapSettings `json:"mapSettings"`
}

type Color struct {
	R float32 `json:"r"`
	G float32 `json:"g"`
	B float32 `json:"b"`
	A float32 `json:"a"`
}

type ResourceCounts struct {
	Food  float64 `json:"food"`
	Wood  float64 `json:"wood"`
	Stone float64 `json:"stone"`
	Metal float64 `json:"metal"`
}

type DisabledTemplates struct {
}

type DisabledTechnologies struct {
}

type Buy struct {
	Food  float64 `json:"food"`
	Wood  float64 `json:"wood"`
	Stone float64 `json:"stone"`
	Metal float64 `json:"metal"`
}

type Sell struct {
	Food  float64 `json:"food"`
	Wood  float64 `json:"wood"`
	Stone float64 `json:"stone"`
	Metal float64 `json:"metal"`
}

type BarterPrices struct {
	Buy  Buy  `json:"buy"`
	Sell Sell `json:"sell"`
}

type ResourcesGathered struct {
	VegetarianFood int `json:"vegetarianFood"`
	Food           int `json:"food"`
	Wood           int `json:"wood"`
	Stone          int `json:"stone"`
	Metal          int `json:"metal"`
}

type Statistics struct {
	ResourcesGathered  ResourcesGathered `json:"resourcesGathered"`
	PercentMapExplored int               `json:"percentMapExplored"`
}

type UnitsTrained struct {
	Infantry      []int `json:"Infantry"`
	Worker        []int `json:"Worker"`
	FemaleCitizen []int `json:"FemaleCitizen"`
	Cavalry       []int `json:"Cavalry"`
	Champion      []int `json:"Champion"`
	Hero          []int `json:"Hero"`
	Siege         []int `json:"Siege"`
	Ship          []int `json:"Ship"`
	Trader        []int `json:"Trader"`
	Domestic      []int `json:"Domestic"`
	Total         []int `json:"total"`
}

type UnitsLost struct {
	Infantry      []int `json:"Infantry"`
	Worker        []int `json:"Worker"`
	FemaleCitizen []int `json:"FemaleCitizen"`
	Cavalry       []int `json:"Cavalry"`
	Champion      []int `json:"Champion"`
	Hero          []int `json:"Hero"`
	Siege         []int `json:"Siege"`
	Ship          []int `json:"Ship"`
	Trader        []int `json:"Trader"`
	Total         []int `json:"total"`
}

type EnemyUnitsKilled struct {
	Infantry      []int `json:"Infantry"`
	Worker        []int `json:"Worker"`
	FemaleCitizen []int `json:"FemaleCitizen"`
	Cavalry       []int `json:"Cavalry"`
	Champion      []int `json:"Champion"`
	Hero          []int `json:"Hero"`
	Siege         []int `json:"Siege"`
	Ship          []int `json:"Ship"`
	Trader        []int `json:"Trader"`
	Total         []int `json:"total"`
}

type UnitsCaptured struct {
	Infantry      []int `json:"Infantry"`
	Worker        []int `json:"Worker"`
	FemaleCitizen []int `json:"FemaleCitizen"`
	Cavalry       []int `json:"Cavalry"`
	Champion      []int `json:"Champion"`
	Hero          []int `json:"Hero"`
	Siege         []int `json:"Siege"`
	Ship          []int `json:"Ship"`
	Trader        []int `json:"Trader"`
	Total         []int `json:"total"`
}

type BuildingsConstructed struct {
	House     []int `json:"House"`
	Economic  []int `json:"Economic"`
	Outpost   []int `json:"Outpost"`
	Military  []int `json:"Military"`
	Fortress  []int `json:"Fortress"`
	CivCentre []int `json:"CivCentre"`
	Wonder    []int `json:"Wonder"`
	Total     []int `json:"total"`
}

type BuildingsLost struct {
	House     []int `json:"House"`
	Economic  []int `json:"Economic"`
	Outpost   []int `json:"Outpost"`
	Military  []int `json:"Military"`
	Fortress  []int `json:"Fortress"`
	CivCentre []int `json:"CivCentre"`
	Wonder    []int `json:"Wonder"`
	Total     []int `json:"total"`
}

type EnemyBuildingsDestroyed struct {
	House     []int `json:"House"`
	Economic  []int `json:"Economic"`
	Outpost   []int `json:"Outpost"`
	Military  []int `json:"Military"`
	Fortress  []int `json:"Fortress"`
	CivCentre []int `json:"CivCentre"`
	Wonder    []int `json:"Wonder"`
	Total     []int `json:"total"`
}

type BuildingsCaptured struct {
	House     []int `json:"House"`
	Economic  []int `json:"Economic"`
	Outpost   []int `json:"Outpost"`
	Military  []int `json:"Military"`
	Fortress  []int `json:"Fortress"`
	CivCentre []int `json:"CivCentre"`
	Wonder    []int `json:"Wonder"`
	Total     []int `json:"total"`
}

type ResourcesUsed struct {
	Food  []int `json:"food"`
	Wood  []int `json:"wood"`
	Stone []int `json:"stone"`
	Metal []int `json:"metal"`
}

type ResourcesSold struct {
	Food  []int `json:"food"`
	Wood  []int `json:"wood"`
	Stone []int `json:"stone"`
	Metal []int `json:"metal"`
}

type ResourcesBought struct {
	Food  []int `json:"food"`
	Wood  []int `json:"wood"`
	Stone []int `json:"stone"`
	Metal []int `json:"metal"`
}

type Sequences struct {
	//UnitsTrained                 UnitsTrained            `json:"unitsTrained"`
	//UnitsLost                    UnitsLost               `json:"unitsLost"`
	UnitsLostValue               []int                   `json:"unitsLostValue"`
	//EnemyUnitsKilled             EnemyUnitsKilled        `json:"enemyUnitsKilled"`
	EnemyUnitsKilledValue        []int                   `json:"enemyUnitsKilledValue"`
	//UnitsCaptured                UnitsCaptured           `json:"unitsCaptured"`
	//UnitsCapturedValue           []int                   `json:"unitsCapturedValue"`
	//BuildingsConstructed         BuildingsConstructed    `json:"buildingsConstructed"`
	//BuildingsLost                BuildingsLost           `json:"buildingsLost"`
	//BuildingsLostValue           []int                   `json:"buildingsLostValue"`
	//EnemyBuildingsDestroyed      EnemyBuildingsDestroyed `json:"enemyBuildingsDestroyed"`
	//EnemyBuildingsDestroyedValue []int                   `json:"enemyBuildingsDestroyedValue"`
	//BuildingsCaptured            BuildingsCaptured       `json:"buildingsCaptured"`
	//BuildingsCapturedValue       []int                   `json:"buildingsCapturedValue"`
	ResourcesGathered            ResourcesGathered       `json:"resourcesGathered"`
	//ResourcesUsed                ResourcesUsed           `json:"resourcesUsed"`
	//ResourcesSold                ResourcesSold           `json:"resourcesSold"`
	//ResourcesBought              ResourcesBought         `json:"resourcesBought"`
	//TributesSent                 []int                   `json:"tributesSent"`
	//TributesReceived             []int                   `json:"tributesReceived"`
	TradeIncome                  []int                   `json:"tradeIncome"`
	//TreasuresCollected           []int                   `json:"treasuresCollected"`
	//LootCollected                []int                   `json:"lootCollected"`
	//PercentMapExplored           []int                   `json:"percentMapExplored"`
	//TeamPercentMapExplored       []int                   `json:"teamPercentMapExplored"`
	//PercentMapControlled         []int                   `json:"percentMapControlled"`
	//TeamPercentMapControlled     []int                   `json:"teamPercentMapControlled"`
	//PeakPercentMapControlled     []int                   `json:"peakPercentMapControlled"`
	//TeamPeakPercentMapControlled []int                   `json:"teamPeakPercentMapControlled"`
	//SuccessfulBribes             []int                   `json:"successfulBribes"`
	//FailedBribes                 []int                   `json:"failedBribes"`
	//Time                         []float64               `json:"time"`
}

type Player struct {
	Name            string         `json:"name"`
	Civ             string         `json:"civ"`
	Color           Color          `json:"color"`
	ControlsAll     bool           `json:"controlsAll"`
	PopCount        int            `json:"popCount"`
	PopLimit        int            `json:"popLimit"`
	PopMax          int            `json:"popMax"`
	PanelEntities   []interface{}  `json:"panelEntities"`
	ResourceCounts  ResourceCounts `json:"resourceCounts"`
	TrainingBlocked bool           `json:"trainingBlocked"`
	// one of "active", "won", "defeated"
	State                string               `json:"state"`
	Team                 int                  `json:"team"`
	TeamsLocked          bool                 `json:"teamsLocked"`
	CheatsEnabled        bool                 `json:"cheatsEnabled"`
	DisabledTemplates    DisabledTemplates    `json:"disabledTemplates"`
	DisabledTechnologies DisabledTechnologies `json:"disabledTechnologies"`
	HasSharedDropsites   bool                 `json:"hasSharedDropsites"`
	HasSharedLos         interface{}          `json:"hasSharedLos"`
	SpyCostMultiplier    float32              `json:"spyCostMultiplier"`
	Phase                string               `json:"phase"`
	IsAlly               []bool               `json:"isAlly"`
	IsMutualAlly         []bool               `json:"isMutualAlly"`
	IsNeutral            []bool               `json:"isNeutral"`
	IsEnemy              []bool               `json:"isEnemy"`
	EntityLimits         interface{}          `json:"entityLimits"`
	EntityCounts         interface{}          `json:"entityCounts"`
	EntityLimitChangers  interface{}          `json:"entityLimitChangers"`
	ResearchQueued       interface{}          `json:"researchQueued"`
	ResearchStarted      interface{}          `json:"researchStarted"`
	ResearchedTechs      interface{}          `json:"researchedTechs"`
	ClassCounts          interface{}          `json:"classCounts"`
	TypeCountsByClass    interface{}          `json:"typeCountsByClass"`
	CanBarter            bool                 `json:"canBarter"`
	BarterPrices         BarterPrices         `json:"barterPrices"`
	Statistics           Statistics           `json:"statistics,omitempty"`
	Sequences            Sequences            `json:"sequences,omitempty"`
}

type MapSettings struct {
	PlayerData        []interface{} `json:"PlayerData"`
	VictoryConditions []string      `json:"VictoryConditions"`
	CircularMap       bool          `json:"CircularMap"`
	Size              int           `json:"Size"`
	PopulationCap     int           `json:"PopulationCap"`
	StartingResources int           `json:"StartingResources"`
	Ceasefire         int           `json:"Ceasefire"`
	RelicCount        int           `json:"RelicCount"`
	RelicDuration     int           `json:"RelicDuration"`
	WonderDuration    int           `json:"WonderDuration"`
	RegicideGarrison  bool          `json:"RegicideGarrison"`
	Nomad             bool          `json:"Nomad"`
	RevealMap         bool          `json:"RevealMap"`
	ExploreMap        bool          `json:"ExploreMap"`
	DisableTreasures  bool          `json:"DisableTreasures"`
	DisableSpies      bool          `json:"DisableSpies"`
	LockTeams         bool          `json:"LockTeams"`
	LastManStanding   bool          `json:"LastManStanding"`
	CheatsEnabled     bool          `json:"CheatsEnabled"`
	RatingEnabled     bool          `json:"RatingEnabled"`
	Name              string        `json:"Name"`
	Script            string        `json:"Script"`
	Description       string        `json:"Description"`
	Preview           string        `json:"Preview"`
	Keywords          []string      `json:"Keywords"`
	TriggerScripts    []string      `json:"TriggerScripts"`
	VictoryScripts    []string      `json:"VictoryScripts"`
	MapType           string        `json:"mapType"`
	Seed              int64         `json:"Seed"`
	AISeed            int64         `json:"AISeed"`
}

// package replay implements the loading of the replay information
//
// 0ad saves games into a custom format, one line per turn, where
// commands are saved so that they can be replayed in the game.
//
// The replay structure is quite easy to parse: break down the
// file line by line, each line has an header describing what comes next;
// a header, a command, a hash.
//
//     replay  := header turn*
//     header  := 'start' Match \n
//     turn    := 'turn' turnNumber durationInMilliseconds '\n' command* 'end' '\n' hash
//     command := 'cmd' player Cmd '\n'
//     hash    := ('hash' | 'hash-quick') hashValue
//
// Where this information is structured it is encoded as JSON.
//
// ## Examples
//
// Every replay, even partials and observers joining after the game has started, start with a header
// containing the information for the game engine to set up the match.
//
//         start {"mapType":"random","settings":{"PlayerData":[{"Name":"Palin (1255)","Civ":"pers","Color":{"r":220,"g":115,"b":16},"Team":0,"AI":false},{"Name":"Laszlord (1312)","Civ":"sele","Color":{"r":231,"g":200,"b":5},"Team":0,"AI":false},{"Name":"edoput (1294)","Civ":"rome","Color":{"r":21,"g":55,"b":149},"Team":0,"AI":false},{"Name":"totoLolo (1362)","Civ":"maur","Color":{"r":86,"g":180,"b":31},"Team":1,"AI":false},{"Name":"warship","Civ":"kush","Color":{"r":64,"g":64,"b":64},"Team":1,"AI":false},{"Name":"Obi (1523)","Civ":"pers","Color":{"r":150,"g":20,"b":20},"Team":1,"AI":false}],"CheatsEnabled":false,"ExploreMap":false,"LockTeams":true,"RevealMap":false,"DisableSpies":false,"DisableTreasures":false,"WorldPopulation":false,"PopulationCap":250,"StartingResources":500,"Ceasefire":0,"VictoryConditions":["conquest"],"Preview":"unknown_continent.png","Nomad":false,"Size":384,"Biome":"generic/savanna","Landscape":"Continent","Seed":1600965042,"AISeed":1921800999,"TriggerScripts":["scripts/TriggerHelper.js","scripts/ConquestCommon.js","scripts/Conquest.js"],"CircularMap":true,"Name":"Unknown"},"gameSpeed":1,"map":"maps/random/unknown","matchID":"07F8A30D52071DE0","script":"unknown.js","timestamp":1620925948,"engine_version":"0.0.24","mods":[["public","0.0.24"]]}
//
// Each turn is described as a list of commands issued by a player, once a turn is finalized it is sent
// to each player so that they can compute changes and then the hash. The hash value is also sent along
// with the turn commands so that the engine can know if the client is out of sync with the host.
//
//         turn 6 500
//         cmd 2 {"type":"construct","template":"structures/sele/farmstead","x":1088.5859375,"z":747.3267211914062,"angle":2.736093730928576,"actorSeed":11035,"entities":[258],"autorepair":true,"autocontinue":true,"queued":false,"formation":"special/formations/null"}
//         cmd 5 {"type":"gather","entities":[366,367,368,369,375],"target":394,"queued":false,"formation":"special/formations/null"}
//         end
//         hash-quick 30dc1585ce779b640d7cb1267cf18efb
//
// ## Usage
//
// The information contained in the replay is a subset of the game metadata that is
// available in the zeroad/summary but is available in one line so it's quite easy
// to parse without reading a whole file.
//
// Loading only the match header is the default option but there are other options:
// - parse turns to get counts
// - parse turns to get raw bytes
// - parse turns to get commands
//
package replay

package replay

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"path"
	"reflect"
	"strconv"
)

// Query is used to select what information to load from the replay.
// Metadata is always loaded as it is cheap, being the first line of
// the file and already a JSON containing useful information, but
// the commands may be too many and that would mean allocating a lot of
// memory and a lot of JSON decoding. Instead of implementing
// two functions let's have one and a simple check.
type Query int

const (
	// Default action is just load the match metadata. This is the fastest option
	// as we will read only the first line of the replay file.
	Metadata Query = iota
	// Go through all commands to populate the Record with StartAt and EndsAt.
	Count
	// Go through all commands to populate also Turn.Commands with a slice command bytes.
	RawBytes
	// Go through all commands to populate also Turns.Commands with a slice of Cmd structs.
	Commands
)

// Record contains the line by line information from the replay file
// as well as some metadata about the available turns.
//
// Each replay has a record of actions but not all replay files
// are complete. If a player rejoin (as a player or as a spectator) then
// the replay starts at the "current turn" and she will receive a
// current snapshot of the world and will start interpreting the commands
// after that to be in sync with the game.
type Record struct {
	// starting turn, 0 indexed
	StartAt int64
	// ending turn, 0 indexed
	EndsAt int64
	// each turn contains a sequence of commands
	// and the corresponding hash of the simulation state
	Turns []Turn
}

// Turn stores the current commands for the turn and the final hash
// of the simulation state; the turn progressive number is its offset
// in the Record.Turn slice + Record.StartAt
type Turn struct {
	Commands interface{}
	Hash     string
}

// LoadDir parses the information for this replay directory
func LoadDir(matchPath string, q Query) (parsed Match, record Record, err error) {
	in, err := os.Open(path.Join(matchPath, "commands.txt"))
	if err != nil {
		return
	}
	defer in.Close()
	parsed, record, err = Load(in, q)
	parsed.path = matchPath
	return
}

// Load parses the information for this replay from r.
//
// TODO: handle empty files as an error
func Load(r io.Reader, q Query) (parsed Match, record Record, err error) {
	parsed.path = reflect.TypeOf(r).String()
	var justMeta = q == Metadata
	var withCommands = (q == RawBytes) || (q == Commands)

	// matches turns are 0 indexed
	var turnNumber int64
	var firstDone bool
	var turnCommands []Cmd
	scanner := bufio.NewScanner(r)
	scanner.Split(bufio.ScanLines)

	for scanner.Scan() {
		b := scanner.Bytes()
		switch {
		// `start` is the starting line of the file and contains the whole replay metadata: players, map,
		// settings
		case bytes.HasPrefix(b, []byte(`start`)):
			index := bytes.IndexByte(b, '{')
			err := json.Unmarshal(b[index:], &parsed)
			if err != nil {
				return parsed, record, fmt.Errorf("match metadata `%s` could not be decoded: %w", parsed.path, err)
			}
			if justMeta {
				return parsed, record, nil
			}
		// `turn` is the line starting a turn; everything between a `turn` and `end`
		// happens in the same turn
		case bytes.HasPrefix(b, []byte(`turn`)):
			// n, m indexes in the current line
			n := bytes.IndexByte(b, ' ') + 1 // next byte after a space
			m := bytes.IndexByte(b[n:], ' ')
			turnNumber, err = strconv.ParseInt(string(b[n:n+m]), 10, 64)
			if err != nil {
				// error parsing the turn number so it makes no sense to have
				// the EndsAt set
				return parsed, record, fmt.Errorf("turn `%v` could not be read", b[n:n+m])
			}
			// now that we have parsed the turn number it is time to
			// store it in the Record if it's the first turn in this
			// replay file
			if !firstDone {
				record.StartAt = turnNumber
				firstDone = true
			}
			if withCommands {
				// a new turn started, allocate a new slice for the commands
				turnCommands = make([]Cmd, 1)
			}
		// `cmd` is the line holding the current command issued by a player
		// many commands may appear in a turn
		case bytes.HasPrefix(b, []byte(`cmd`)):
			if !withCommands {
				continue
			}
			// reading the player number
			index := bytes.IndexByte(b, ' ') + 1
			b = b[index:]
			index = bytes.IndexByte(b, ' ')
			_, err = strconv.Atoi(string(b[:index]))
			if err != nil {
				return parsed, record, fmt.Errorf("player number could not be found: %w\n", err)
			}
			// reading the command
			index = bytes.IndexByte(b, '{')
			var currentCmd Cmd
			err := json.Unmarshal(b[index:], &currentCmd)
			if err != nil {
				return parsed, record, fmt.Errorf("command `%s` could not be decoded: %w\n", b[index:], err)
			}
			turnCommands = append(turnCommands, currentCmd)
		// `end` the current turn
		case bytes.HasPrefix(b, []byte(`end`)):
			continue
		// `hash-*` describe the current simulation state and is used to rule
		// out of sync players
		case bytes.HasPrefix(b, []byte(`hash`)) || bytes.HasPrefix(b, []byte(`hash-quick`)):
			if !withCommands {
				continue
			}
			index := bytes.IndexByte(b, ' ') + 1
			record.Turns = append(record.Turns, Turn{Commands: turnCommands, Hash: string(b[index:])})
		default:
			continue
		}

	}
	// also store the last number in our replay file in the Record
	record.EndsAt = turnNumber
	return parsed, record, nil
}

package replay

type difficulty int

const (
	notSpecified difficulty = iota
	sandbox
	easy
	medium
	hard
	veryHard
)

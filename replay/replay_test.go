package replay

import (
        "testing"
        "strings"
)

// TODO: Load should return an error with an empty file
func TestLoadEmpty(t *testing.T) {
        header := ""
        r := strings.NewReader(header)
        _, _, err := Load(r, Metadata)
        if err != nil {
                t.Error(err)
        }
}

func TestLoadMetadata(t *testing.T) {
        header := `start {"settings":{"PlayerData":[{"Name":"andre92 (1472)","Civ":"sele","Team":0,"AI":false,"Color":{"r":50,"g":170,"b":170}},{"Name":"Palin (1255)","Civ":"kush","Team":0,"AI":false,"Color":{"r":220,"g":115,"b":16}},{"Name":"slashit","Civ":"cart","Color":{"r":150,"g":20,"b":20},"Team":0,"AI":false},{"Name":"edoput (1325)","Civ":"sele","Color":{"r":21,"g":55,"b":149},"Team":0,"AI":false},{"Name":"clavz (1381)","Civ":"athen","Color":{"r":86,"g":180,"b":31},"Team":1,"AI":false},{"Name":"reaction","Civ":"rome","Color":{"r":231,"g":200,"b":5},"Team":1,"AI":false},{"Name":"alre (1354)","Civ":"maur","Color":{"r":160,"g":80,"b":200},"Team":1,"AI":false},{"Name":"Sir_Atheas","Civ":"pers","Color":{"r":64,"g":64,"b":64},"Team":1,"AI":false}],"CheatsEnabled":false,"DisableSpies":false,"DisableTreasures":false,"ExploreMap":false,"LockTeams":true,"RevealMap":false,"WorldPopulation":false,"PopulationCap":200,"StartingResources":500,"Ceasefire":0,"VictoryConditions":["conquest"],"Preview":"unknown_passes.png","Nomad":false,"Size":384,"Biome":"generic/savanna","Landscape":"Passes","Seed":1370241353,"AISeed":3782172846,"TriggerScripts":["scripts/TriggerHelper.js","scripts/ConquestCommon.js","scripts/Conquest.js"],"CircularMap":true,"Name":"Unknown"},"gameSpeed":1,"mapType":"random","map":"maps/random/unknown","matchID":"9C0FE82C32D3B6C8","script":"unknown.js","timestamp":1622646447,"engine_version":"0.0.24","mods":[["public","0.0.24"]]}`

        r := strings.NewReader(header)
        _, _, err := Load(r, Metadata)
        if err != nil {
                t.Error(err)
        }
}

func TestLoadCount0(t *testing.T) {
        header := `start {"settings":{"PlayerData":[{"Name":"andre92 (1472)","Civ":"sele","Team":0,"AI":false,"Color":{"r":50,"g":170,"b":170}},{"Name":"Palin (1255)","Civ":"kush","Team":0,"AI":false,"Color":{"r":220,"g":115,"b":16}},{"Name":"slashit","Civ":"cart","Color":{"r":150,"g":20,"b":20},"Team":0,"AI":false},{"Name":"edoput (1325)","Civ":"sele","Color":{"r":21,"g":55,"b":149},"Team":0,"AI":false},{"Name":"clavz (1381)","Civ":"athen","Color":{"r":86,"g":180,"b":31},"Team":1,"AI":false},{"Name":"reaction","Civ":"rome","Color":{"r":231,"g":200,"b":5},"Team":1,"AI":false},{"Name":"alre (1354)","Civ":"maur","Color":{"r":160,"g":80,"b":200},"Team":1,"AI":false},{"Name":"Sir_Atheas","Civ":"pers","Color":{"r":64,"g":64,"b":64},"Team":1,"AI":false}],"CheatsEnabled":false,"DisableSpies":false,"DisableTreasures":false,"ExploreMap":false,"LockTeams":true,"RevealMap":false,"WorldPopulation":false,"PopulationCap":200,"StartingResources":500,"Ceasefire":0,"VictoryConditions":["conquest"],"Preview":"unknown_passes.png","Nomad":false,"Size":384,"Biome":"generic/savanna","Landscape":"Passes","Seed":1370241353,"AISeed":3782172846,"TriggerScripts":["scripts/TriggerHelper.js","scripts/ConquestCommon.js","scripts/Conquest.js"],"CircularMap":true,"Name":"Unknown"},"gameSpeed":1,"mapType":"random","map":"maps/random/unknown","matchID":"9C0FE82C32D3B6C8","script":"unknown.js","timestamp":1622646447,"engine_version":"0.0.24","mods":[["public","0.0.24"]]}`

        r := strings.NewReader(header)
        _, turns, err := Load(r, Count)
        if err != nil {
                t.Error(err)
        }
        if turns.StartAt != 0 && turns.EndsAt != 0 {
                t.Fatal("Replay does not have turns but turns range is not [0,0]")
        }
}

func TestLoadCount1(t *testing.T) {
        header := `start {"settings":{"PlayerData":[{"Name":"andre92 (1472)","Civ":"sele","Team":0,"AI":false,"Color":{"r":50,"g":170,"b":170}},{"Name":"Palin (1255)","Civ":"kush","Team":0,"AI":false,"Color":{"r":220,"g":115,"b":16}},{"Name":"slashit","Civ":"cart","Color":{"r":150,"g":20,"b":20},"Team":0,"AI":false},{"Name":"edoput (1325)","Civ":"sele","Color":{"r":21,"g":55,"b":149},"Team":0,"AI":false},{"Name":"clavz (1381)","Civ":"athen","Color":{"r":86,"g":180,"b":31},"Team":1,"AI":false},{"Name":"reaction","Civ":"rome","Color":{"r":231,"g":200,"b":5},"Team":1,"AI":false},{"Name":"alre (1354)","Civ":"maur","Color":{"r":160,"g":80,"b":200},"Team":1,"AI":false},{"Name":"Sir_Atheas","Civ":"pers","Color":{"r":64,"g":64,"b":64},"Team":1,"AI":false}],"CheatsEnabled":false,"DisableSpies":false,"DisableTreasures":false,"ExploreMap":false,"LockTeams":true,"RevealMap":false,"WorldPopulation":false,"PopulationCap":200,"StartingResources":500,"Ceasefire":0,"VictoryConditions":["conquest"],"Preview":"unknown_passes.png","Nomad":false,"Size":384,"Biome":"generic/savanna","Landscape":"Passes","Seed":1370241353,"AISeed":3782172846,"TriggerScripts":["scripts/TriggerHelper.js","scripts/ConquestCommon.js","scripts/Conquest.js"],"CircularMap":true,"Name":"Unknown"},"gameSpeed":1,"mapType":"random","map":"maps/random/unknown","matchID":"9C0FE82C32D3B6C8","script":"unknown.js","timestamp":1622646447,"engine_version":"0.0.24","mods":[["public","0.0.24"]]}
turn 10 500
end
hash-quick deadbeef`
        t.Log(header)

        r := strings.NewReader(header)
        _, turns, err := Load(r, Count)
        if err != nil {
                t.Error(err)
        }
        if turns.StartAt != 10 && turns.EndsAt != 10 {
                t.Fatalf("Replay turns range is [10,10] got [%d,%d]", turns.StartAt, turns.EndsAt)
        }
}

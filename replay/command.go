package replay

import (
	"fmt"
)

type team int

const (
	noteam team = -1
)

type Match struct {
	// the settings define the rules of this match
	// along with the map and the players
	Settings Settings `json:"settings"`
	GameName string   `json:"gameName"`
	Map      string   `json:"map"`
	// this might be the only interesting piece of
	// metadata in here; I'm not sure why they
	// would put it in the replay when there's the
	// metadata.json but ok
	MatchID string `json:"matchID"`
	// when the replay ended
	Timestamp     int64      `json:"timestamp"`
	EngineVersion string     `json:"engine_version"`
        // a25 changed the mods from [['public': '0.0.24']]
        // to [ {...} ] so either I make a custom type for
        // deserialization or I should loose this
	//Mods          [][]string `json:"mods"`
	// internal fields
	// path to the file from which we replay.Load
	path string
}

func (m Match) Path() string {
	return m.path
}

type Settings struct {
	Players           []Player `json:"PlayerData"`
	VictoryConditions []string `json:"VictoryConditions"`
	CircularMap       bool     `json:"CircularMap"`
	Size              int      `json:"Size"`
	PopulationCap     int      `json:"PopulationCap"`
	StartingResources int      `json:"StartingResources"`
	Ceasefire         int      `json:"Ceasefire"`
	RelicCount        int      `json:"RelicCount"`
	RelicDuration     int      `json:"RelicDuration"`
	WonderDuration    int      `json:"WonderDuration"`
	RegicideGarrison  bool     `json:"RegicideGarrison"`
	Nomad             bool     `json:"Nomad"`
	RevealMap         bool     `json:"RevealMap"`
	ExploreMap        bool     `json:"ExploreMap"`
	DisableTreasures  bool     `json:"DisableTreasures"`
	DisableSpies      bool     `json:"DisableSpies"`
	LockTeams         bool     `json:"LockTeams"`
	LastManStanding   bool     `json:"LastManStanding"`
	CheatsEnabled     bool     `json:"CheatsEnabled"`
	RatingEnabled     bool     `json:"RatingEnabled"`
	Name              string   `json:"Name"`
	Script            string   `json:"script"`
	Description       string   `json:"Description"`
	Preview           string   `json:"Preview"`
	Keywords          []string `json:"Keywords"`
	Biome             string   `json:"Biome"`
	TriggerScripts    []string `json:"TriggerScripts"`
	VictoryScripts    []string `json:"VictoryScripts"`
	MapType           string   `json:"mapType"`
	Seed              int64    `json:"Seed"`
	AISeed            int64    `json:"AISeed"`
}

type Player struct {
	Name       string      `json:"Name"`
	Civ        string      `json:"Civ"`
	AI         interface{} `json:"AI"`
	AIDiff     difficulty  `json:"AIDiff"`
	AIBehavior string      `json:"AIBehavior"`
	Team       team        `json:"Team"`
}

func (p Player) Bot() bool {
	switch v := p.AI.(type) {
	case bool:
		return v
	case string:
		return v != ""
	default:
		return false
	}
}

func (p Player) BotName() string {
	return fmt.Sprintf("%s/%s", p.AI, p.AIDiff)
}

// the json package only accesses the exported fields of struct
// types

// func Unmarshal(data, []byte, v interface{}) error
// Unmarshal identify the fields in which to store
// the decoded data in this order
//
// 1. exported field with tags
// 2. exported field with same name
// 3. exported field case insensitive
//
// if it does not fit Unmarshal will decode only the fields that
// it can find in the destination type
//
// # Generic json with interface
// the json package uses map[string]interface{} and []interface{}
// values to store arbitrary JSON objects and arrays; it will happily
// unmarshal any valid JSON blob into a plain interface{} value.
//
// If one field is nil and you try to Unmarshal into it then
// a new slice will be allocated behind the scenes for supported
// reference types (pointers, slices and maps).

// this should be the scalable approach for our commands; when unmarshaling
// only the fields present in the JSON will be copied over.
// this makes it possible to have { "type" : "resign" }
// populate only the Kind field and { "type" : "train", "entries": [1] ...}
// populate Entries when needed

type Cmd struct {
	Issuer string `json:-`
	// the command kind can be one of
	// attack, attack-walk, barter, construct,
	// delete-entities, garrison, gather
	// pack, repair, research, resign
	// returnresource, train, tribute, walk,
	// unload-all, unload-template, upgrade
	Kind string `json:"type"`
	// Executor
	// issue the command to this entities
	Entities []int `json:"entities"`
	Holder   []int `json:"garrisonHolders"`
	Player   int   `json:player`
	// Target
	// the data associated to our command
	Template string  `json:"template"`
	Count    int     `json:"count"`
	X        float32 `json:"x"`
	Z        float32 `json:"z"`
	Angle    float32 `json:"angle"`
	// Options
	Autorepair   bool `json:"autorepair"`
	Autocontinue bool `json:"autocontinue"`
	Capture      bool `json:"allowCapture"`
	Queued       bool `json:"queued"`
	Pack         bool `json:"pack"`
	Owner        int  `json:"owner"`
}

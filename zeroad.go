// Package zeroad implements the building blocks to read
// the data saved in the 0ad replay folder.
//
// There are two files in a replay, `commands.txt` and `metadata.json`,
// that carry the game turns and the final data that the players
// would see in the ``Summary'' page.
//
// We can load a replay data using `replay.Load` and a summary using `summary.Load`
// by passing the path to the match directory.
//
//      package main
//
//      imports (
//              "filepath/path"
//              "zeroad"
//              "zeroad/replay"
//      )
//
//      func main() {
//              ...
//              matchMeta, _, err := replay.Load(matchPath, replay.Metadata)
//              matchSummary,err := summary.Load(matchPath)
//      }
//
// As 0ad stores all the replays under the game data path
// there is also the possibility to iterate over all the replay directories using `zeroad.WalkMatch`
//
//      // ReadMeta will save all the replay metadata in the data slice
//      func ReadMeta (data *[]replay.Match) fs.WalkFunc {
//              return func (matchPath string, info fs.DirEntry, err error) error {
//                      match, _, err := replay.Load(matchPath, replay.Metadata)
//                      *data = append(*data, match)
//              }
//      }

package zeroad

import (
	"fmt"
	"io/fs"
	"os"
	"path"
	"path/filepath"
	"regexp"
	"runtime"
	"strings"
)

var (
	matchNameR *regexp.Regexp
)

func init() {
	matchNameR = regexp.MustCompile(`\d{4}-\d{2}-\d{2}_\d{4}`)
}

func replayDir() (string, error) {
	homeDir, err := os.UserHomeDir()
	if err != nil {
		return homeDir, err
	}
	os := runtime.GOOS
	switch os {
	case "windows":
		return path.Join(homeDir, "Documents", "My Games", "0ad", "replays"), nil
	case "darwin":
		return path.Join(homeDir, "Library", "Application Support", "0ad", "replays"), nil
	case "linux":
		return path.Join(homeDir, ".local", "share", "0ad", "replays"), nil
	default:
		return "", fmt.Errorf("OS not recognised")
	}
}

// WalkMatch walks the file tree containing replays calling fn for each match directory.
//
// All errors that arise visiting files and directories are filtered by fn: see the WalkDirFunc documentation for details.
//
// WalkMatch does not follow symbolic links.
func WalkMatch(fn fs.WalkDirFunc) error {
	// here we can filter the tree to make sure only the match
	// directories are handled by fn
	root, err := replayDir()
	if err != nil {
		return fmt.Errorf("could not guess replay directory: %w", err)
	}

	// the replay tree is the following
	// ~/.local/share/0ad/{{version}}/
	//                                replayChache.json
	//                                yyyy-mm-dd_{{serial}}/
	//                                                      commands.txt
	//                                                      metadata.json
	// data from different versions, e.g. 0.0.23 or 0.0.24, is stored in
	// separate directories and each match has its own directory
	// with the yyyy-mm-dd_{{serial}} naming schema
	t := func(path string, info fs.DirEntry, err error) error {
		// we are looking for directories so this is convenient
		if !info.IsDir() {
			return fs.SkipDir
		}
		// and only directories that match this naming schema
		if matchNameR.MatchString(filepath.Base(path)) {
			return fn(path, info, err)
		}
		// otherwise just return nil and continue the iteration
		return nil

	}
	return filepath.WalkDir(root, t)
}

// Normalize removes any mentions of the ranking in the player name.
func Normalize(playerName string) string {
	rankingStarts := strings.IndexRune(playerName, ' ') // the lobby uses whitespace to separate player name and ranking
	if rankingStarts == -1 {
		return playerName
	} else {
		return playerName[0 : rankingStarts-1]
	}
}
